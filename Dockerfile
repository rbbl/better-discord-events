FROM gradle:8.1.1-jdk17 as builder
COPY gradle /app/gradle
COPY build.gradle.kts /app/
COPY gradle.properties /app/
COPY gradlew* /app/
COPY settings.gradle.kts /app/
COPY src/ /app/src
WORKDIR /app
RUN gradle build

FROM eclipse-temurin:17
COPY --from=builder /app/build/libs/yoinker.jar /app/yoinker.jar
ENTRYPOINT java -jar /app/yoinker.jar
