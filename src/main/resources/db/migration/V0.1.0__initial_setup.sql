CREATE TABLE event_group (
    event_id bigint PRIMARY KEY,
    group_id bigint,
    active bool default false
)