package cc.rbbl.persistence

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID

class EventGroup(id: EntityID<ULong>) : Entity<ULong>(id) {
    companion object : EntityClass<ULong, EventGroup>(EventGroupTable)

    var groupId by EventGroupTable.groupId
    var active by EventGroupTable.active
}