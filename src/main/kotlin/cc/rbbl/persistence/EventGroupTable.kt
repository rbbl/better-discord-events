package cc.rbbl.persistence

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column

object EventGroupTable : IdTable<ULong>("event_group") {
    private val eventId = ulong("event_id")
    val groupId = ulong("group_id")
    val active = bool("active").default(true)
    override val id: Column<EntityID<ULong>>
        get() = eventId.entityId()
}