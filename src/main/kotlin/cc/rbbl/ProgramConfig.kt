package cc.rbbl

import cc.rbbl.program_parameters_jvm.ParameterDefinition
import cc.rbbl.program_parameters_jvm.ParameterHolder

class ProgramConfig {
    companion object {
        val parameters = ParameterHolder(
            setOf(
                ParameterDefinition("DISCORD_TOKEN"),
                ParameterDefinition("DB_USER"),
                ParameterDefinition("DB_PASSWORD"),
                ParameterDefinition("JDBC_URL"),
                ParameterDefinition("WEBSERVER_ENABLED", false),
                ParameterDefinition("PORT", false),
                ParameterDefinition("STATS_CACHE_TIME_MS", false)
            )
        )
    }

    val discordToken: String = parameters["DISCORD_TOKEN"]!!
    val dbUser: String = parameters["DB_USER"]!!
    val dbPassword: String = parameters["DB_PASSWORD"]!!
    val jdbcUrl: String = parameters["JDBC_URL"]!!
}