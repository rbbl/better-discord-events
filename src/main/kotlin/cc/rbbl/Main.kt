package cc.rbbl

import cc.rbbl.persistence.EventGroup
import dev.kord.common.entity.Snowflake
import dev.kord.core.Kord
import dev.kord.core.event.guild.GuildScheduledEventCreateEvent
import dev.kord.core.event.guild.GuildScheduledEventDeleteEvent
import dev.kord.core.on
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory


private val log = LoggerFactory.getLogger("Main")

suspend fun main(args: Array<String>) {
    ProgramConfig.parameters.loadParametersFromEnvironmentVariables()
    ProgramConfig.parameters.loadParameters(args)
    ProgramConfig.parameters.checkParameterCompleteness()

    val config = ProgramConfig()

    handleDbMigration(config)
    Database.connect(config.jdbcUrl, "org.postgresql.Driver", config.dbUser, config.dbPassword)

    val kord = Kord(config.discordToken)

    kord.on<GuildScheduledEventCreateEvent> {
        val eventId = this.scheduledEventId
        val eventName = this.scheduledEvent.name
        val guildId = this.scheduledEvent.guildId
        val groupResponse = kord.rest.guild.createGuildRole(guildId) {
            this.name = eventName
            this.mentionable = true
        }
        transaction {
            EventGroup.new(eventId.value) {
                groupId = groupResponse.id.value
            }
        }
    }

    kord.on<GuildScheduledEventDeleteEvent> {
        val event = this
        val groupEvent = transaction {
            val eventGroup = EventGroup.findById(event.scheduledEventId.value)
            eventGroup?.active = false
            eventGroup
        }
        if (groupEvent != null) {
            kord.rest.guild.deleteGuildRole(event.guildId, Snowflake(groupEvent.groupId))
        } else {
            log.warn("could not find event ${event.scheduledEventId} in Database")
        }
    }

    kord.login {
    }
}

private fun handleDbMigration(config: ProgramConfig) {
    val flyway = Flyway.configure().dataSource(
        config.jdbcUrl,
        config.dbUser,
        config.dbPassword
    ).load()
    flyway.migrate()
}